export function emailValidation(email: string) {
  if (havePersian(email)) return false;
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
export function havePersian(st: string) {
  const str_to_arr_of_UTF8 = new TextEncoder().encode(st);
  const filter = str_to_arr_of_UTF8.filter((e) => e > 128 && e < 220);
  return filter.length > 0;
}
