import Head from "next/head";
import styles from "@/styles/Chat.module.css";
import { getStatePersist, useStorePersistShallow } from "@/zustand/Store";
import { useEffect, useState } from "react";
import { useChat } from "@/pages/api/chat";
import { ChatType, ServerProptType } from "@/components/chat/Chat.props";
import LoadingDot from "@/components/loading/LoadingDot";
import Cookies from "js-cookie";

function Home() {
  const [textArea, setTextArea] = useState<string>();
  const [name] = useState<string | undefined>(Cookies.get("name"));
  const [user, setUser] =
    useState<{ date: string; text: string; owner: 1 | 2 }[]>();
  const chats: ChatType = useStorePersistShallow((state) => state.chats);
  const setChats = getStatePersist("setChats");

  useEffect(() => {
    if (!chats || !name || !chats[name]) return;
    setUser(
      chats[name].sort(
        (a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()
      )
    );
  }, [chats]); // eslint-disable-line react-hooks/exhaustive-deps

  const { data, mutate: add, isLoading } = useChat();

  useEffect(() => {
    const server: ServerProptType = data;
    addComment(server?.msg, 1);
  }, [data]); // eslint-disable-line react-hooks/exhaustive-deps

  function onSendClick() {
    if (!textArea) return;
    add({ text: textArea });
    addComment(textArea, 2);
    setTextArea("");
  }

  function addComment(comment: string, owner: number) {
    if (!comment || !name) return;
    setChats({
      ...chats,
      [name]: [
        ...(user || []),
        {
          date: new Date(),
          text: comment,
          owner: owner,
        },
      ],
    });
  }

  return (
    <>
      <Head>
        <title>Chat with us</title>
        <meta name="description" content="this page is for chatting assist" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <div className={styles.bg}>
          <div className={styles.title}>chat</div>
          <div className={styles.face}>
            <div className={styles.map}>
              {user?.map((user) => (
                <div key={user.date} className={styles.row}>
                  {user.owner === 1 && (
                    <div>
                      <div className={styles.left}>{user.text}</div>
                      <div className={styles.namel}>Assist</div>
                    </div>
                  )}
                  {user.owner === 2 && (
                    <div className={styles.bubble}>
                      <div className={styles.right}>{user.text}</div>
                      <div className={styles.namer}>{name}</div>
                    </div>
                  )}
                </div>
              ))}
            </div>
          </div>
          {isLoading && (
            <div className={styles.loading}>
              <LoadingDot />
            </div>
          )}

          <div className={styles.inputMain}>
            <textarea
              className={styles.input}
              value={textArea}
              onChange={(e) => setTextArea(e.target.value)}
            />
            <div className={styles.sendParent} onClick={onSendClick}>
              <div className={styles.send} />
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default Home;
