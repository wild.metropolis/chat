export type ChatType = {
  [user: string]: { date: string; text: string; owner: 1 | 2 }[];
};

export type ServerProptType = {
  created_at: string;
  msg: string;
  openai_id: string;
  role: string;
};
