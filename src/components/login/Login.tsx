import { useEffect, useState } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import * as querystring from "querystring";
import styles from "@/styles/Login1.module.css";
import { getState } from "@/zustand/Store";
import { useLogin } from "@/pages/api/login";
import { emailValidation } from "@/utils/Validations";
import Link from "next/link";
import cookie from "js-cookie";

const Button = dynamic(() => import("../common/button"), { ssr: false });
const Input = dynamic(() => import("../common/input"), {
  ssr: false,
});

function Login() {
  const router = useRouter();
  const { mutate: sendOtp, isLoading, isSuccess, data } = useLogin();

  const query = querystring.parse(router.asPath.split("?")[1]);

  const [email, setEmail] = useState<string>(
    typeof query.email === "string" ? query.email : ""
  );
  const [pass, setPass] = useState<string>();

  useEffect(() => {
    if (isSuccess) {
      const date = new Date();
      date.setTime(+date + 60 * 86400000);
      cookie.set("auth", data.access_token, { expires: date });
      cookie.set("name", email);
      router.push(`/chat`).then();
    }
  }, [isSuccess]); // eslint-disable-line react-hooks/exhaustive-deps

  function handleSubmit() {
    if (!email || email?.length < 4 || !emailValidation(email)) {
      const setNotify = getState("setNotify");
      setNotify({
        type: "warning",
        text: "please check email address",
      });
      return;
    }
    if (!pass) {
      const setNotify = getState("setNotify");
      setNotify({
        type: "warning",
        text: "fill password and re-password inputs",
      });
      return;
    }
    if (pass?.length < 6) {
      const setNotify = getState("setNotify");
      setNotify({
        type: "warning",
        text: "please check password min Length is 6",
      });
      return;
    }
    sendOtp({ email, pass });
  }

  function handleKeyDown(e: any) {
    if (e.key === "Enter") handleSubmit();
  }

  return (
    <div className={styles.lnMain}>
      <div className={styles.lnL00}>
        <div className={styles.lnL1}>
          <div className={styles.lnL1L1}></div>
          <div className={styles.lnL1L2} style={{ flex: 1 }}></div>
          <div className={styles.lnL1L3}></div>
        </div>
        <div className={styles.lnL3}>
          <Input
            onKeyDown={handleKeyDown}
            ltr={1}
            placeholder={"Email Address"}
            value={email}
            onChange={(v) => {
              setEmail(v.target.value);
            }}
          />
        </div>
        <div className={styles.lnL3} style={{ marginTop: 12 }}>
          <Input
            onKeyDown={handleKeyDown}
            minLength={6}
            ltr={1}
            type="password"
            placeholder={"Password"}
            value={pass}
            onChange={(v) => {
              setPass(v.target.value);
            }}
          />
        </div>

        <div className={styles.lnL6}></div>
        <div className={styles.lnL8}>
          <Button
            className={styles.lnL9}
            loading={isLoading ? 1 : 0}
            onClick={() => handleSubmit()}
            title={"Login"}
          />

          <Link href={"/signup"} className={styles.lnL9}>
            Create an account
          </Link>
          <Link href={"/forgot"} className={styles.lnL10}>
            Forgot Password
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Login;
