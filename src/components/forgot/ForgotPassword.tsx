import { useEffect, useState } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import * as querystring from "querystring";
import styles from "@/styles/Login1.module.css";
import { getState } from "@/zustand/Store";
import { useForgotPass } from "@/pages/api/login";
import { emailValidation } from "@/utils/Validations";

const Button = dynamic(() => import("../common/button"), { ssr: false });
const Input = dynamic(() => import("../common/input"), {
  ssr: false,
});

function ForgotPassword() {
  const [state, setState] = useState<number>(1);

  const router = useRouter();
  const { mutate: sendForgotPass, isLoading, isSuccess } = useForgotPass();

  const query = querystring.parse(router.asPath.split("?")[1]);

  const [email, setEmail] = useState<string>(
    typeof query.email === "string" ? query.email : ""
  );

  useEffect(() => {
    if (isSuccess) {
      setState(2);
    }
  }, [isSuccess]); // eslint-disable-line react-hooks/exhaustive-deps

  function handleSubmit() {
    if (!email || email?.length < 4 || !emailValidation(email)) {
      const setNotify = getState("setNotify");
      setNotify({
        type: "warning",
        text: "please check email address",
      });
      return;
    }
    sendForgotPass({ email });
  }

  function handleKeyDown(e: any) {
    if (e.key === "Enter") handleSubmit();
  }

  if (state === 2) {
    return (
      <div className={styles.lnMain}>
        <div className={styles.lnL00}>
          <div className={styles.lnL12}>
            Look for the verification email in your inbox and click the link in
            that email. A confirmation message will appear in your web browser.
          </div>
          <div className={styles.lnL11}>
            <div className={styles.lnL9} onClick={handleKeyDown}>
              Didnt get the email?
            </div>
            <div className={styles.lnL9} onClick={() => setState(1)}>
              Back
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className={styles.lnMain}>
      <div className={styles.lnL00}>
        <div className={styles.lnL1}>
          <div className={styles.lnL1L1}></div>
          <div className={styles.lnL1L2} style={{ flex: 1 }}></div>
          <div className={styles.lnL1L3}></div>
        </div>
        <div className={styles.lnL3}>
          <Input
            onKeyDown={handleKeyDown}
            ltr={1}
            placeholder={"Email Address"}
            value={email}
            onChange={(v) => {
              setEmail(v.target.value);
            }}
          />
        </div>

        <div className={styles.lnL6}></div>
        <div className={styles.lnL8}>
          <Button
            className={styles.lnL9}
            loading={isLoading ? 1 : 0}
            onClick={() => handleSubmit()}
            title={"Continue"}
          />
        </div>
      </div>
    </div>
  );
}

export default ForgotPassword;
