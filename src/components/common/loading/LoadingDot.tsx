import style from "./Loading.module.css";
import * as React from "react";

function LoadingDot() {
  return (
    <div className={style.ldsEllipsis}>
      <div className={style.ldsEllipsisDiv} />
      <div className={style.ldsEllipsisDiv} />
      <div className={style.ldsEllipsisDiv} />
      <div className={style.ldsEllipsisDiv} />
    </div>
  );
}

export default LoadingDot;
