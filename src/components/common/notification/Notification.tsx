import React, { useEffect } from "react";
// @ts-ignore
import { useSnackbar } from "react-simple-snackbar";
import { useStore } from "../../../zustand/Store";

export default function Notification() {
  const [openError] = useSnackbar({
    position: "bottom-right",
    style: {
      fontFamily: "iranyekan",
      textAlign: "right",
      color: "lightcoral",
    },
    closeStyle: {
      color: "lightcoral",
      fontSize: "14px",
    },
  });
  const [openSuccess] = useSnackbar({
    position: "bottom-right",
    style: {
      fontFamily: "iranyekan",
      textAlign: "right",
      color: "#0bf717",
    },
    closeStyle: {
      color: "#0bf717",
      fontWeight: 700,
      fontSize: "14px",
    },
  });

  const [openWarning] = useSnackbar({
    position: "bottom-right",

    style: {
      fontFamily: "iranyekan",
      textAlign: "right",
      color: "#FFCC00",
    },
    closeStyle: {
      color: "#FFCC00",
      fontWeight: 700,
      fontSize: "14px",
    },
  });

  const notify = useStore((state) => state.notify);

  useEffect(() => {
    if (notify && notify.text) {
      if (notify?.type === "error") openError(notify.text);
      else if (notify?.type === "warning") openWarning(notify.text);
      else openSuccess(notify.text);
    }
  }, [notify]); // eslint-disable-line react-hooks/exhaustive-deps

  return <></>;
}
