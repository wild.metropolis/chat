import * as React from "react";
import styles from "./Input.module.css";
import { InputHTMLAttributes } from "react";
import { ExtraInputType } from "./Input.props";

const Input = (
  props: InputHTMLAttributes<HTMLInputElement> & ExtraInputType
) => {
  const ltr = !!props.ltr;
  return (
    <div
      className={`${styles.inputMain || ""} ${props.className || ""} ${
        ltr ? styles.myInputLeft : styles.myInputRight
      }`}
      style={{ ...{ height: props?.subtext ? 78 : 60 }, ...props?.style }}
    >
      {(props.value || props.value == "0") && (
        <>
          <div
            className={styles.topplaceholder}
            style={{
              alignSelf: "flex-start",
              ...(props?.topplaceholderstyle || {}),
            }}
          >
            <div className={styles.topplaceholderback} />
            <div className={styles.topplaceholdertext}>
              {props?.placeholder || ""}
            </div>
          </div>
        </>
      )}
      <input
        aria-label={props.placeholder || props["aria-label"]}
        {...props}
        className={`${styles.myInput || ""} ${props.inputclassname || ""} ${
          ltr ? styles.myInputLeft : styles.myInputRight
        }`}
        style={props?.inputstyle || {}}
      />
      {props?.subtext && (
        <div
          className={`${styles.subtext || ""} `}
          style={props?.subtextstyle || {}}
        >
          {props?.subtext || ""}
        </div>
      )}
    </div>
  );
};
export default Input;
