import * as React from "react";

export type ExtraInputType = {
  subtext?: string;
  subtextstyle?: React.CSSProperties;
  inputstyle?: React.CSSProperties;
  topplaceholderstyle?: React.CSSProperties;
  inputclassname?: string | undefined;
  ltr?: 0 | 1;
};
