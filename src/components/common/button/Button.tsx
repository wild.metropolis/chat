import * as React from "react";
import { ButtonHTMLAttributes } from "react";
import dynamic from "next/dynamic";

const Loading = dynamic(() => import("../loading"), { ssr: false });

function Button(
  props: ButtonHTMLAttributes<HTMLButtonElement> & {
    icon?: any;
    title?: string;
    contentstyle?: React.CSSProperties;
    loading?: 0 | 1;
    contentclass?: string;
  }
) {
  const loading = !!props.loading;
  const newProps = {
    ...props,
    style: {
      backgroundColor: "rgb(0 124 207)",
      color: "#fff",
      height: 48,
      minHeight: 48,
      border: 0,
      ...(props.style || {}),
    },
  };

  return (
    <>
      <button
        {...newProps}
        onClick={(e) => {
          if (!loading) {
            props.onClick && props.onClick(e);
          }
        }}
      >
        <div
          className={props.contentclass}
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            alignContent: "center",
            lineHeight: "24px",
            fontSize: 16,
            fontWeight: 700,
            pointerEvents: "none",
            ...props.contentstyle,
          }}
        >
          {!loading && props.title}
          {!loading && props.icon && <span style={{ width: 8 }} />}
          {!loading && props.icon}
          {loading && <Loading />}
        </div>
      </button>
    </>
  );
}

export default Button;
