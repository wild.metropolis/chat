import * as React from "react";
import { ButtonHTMLAttributes } from "react";
import dynamic from "next/dynamic";

const Button = dynamic(() => import("./Button"), { ssr: false });

function ButtonWhite(
  props: ButtonHTMLAttributes<HTMLButtonElement> & {
    icon?: any;
    title?: string;
    loading?: 0 | 1;
    contentstyle?: React.CSSProperties;
    rightcontent?: 1 | 0;
  }
) {
  return (
    <Button
      {...props}
      contentstyle={
        props.rightcontent
          ? {
              placeContent: "flex-end",
              direction: "ltr",
              fontSize: 12,
              ...(props.contentstyle || {}),
            }
          : props.contentstyle || {}
      }
      style={{
        backgroundColor: "#fff",
        color: "#000",
        border: "1px solid #000000",
        ...(props.style || {}),
      }}
    />
  );
}

export default ButtonWhite;
