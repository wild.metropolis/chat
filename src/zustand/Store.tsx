import { create, GetState, SetState } from "zustand";
import { devtools, persist } from "zustand/middleware";
import { ChatType } from "@/components/chat/Chat.props";

type nameSpace = {
  chats: ChatType;
  notify?: { type: "error" | "success" | "warning"; text: string };
};

let store = (set: SetState<nameSpace>, get: GetState<nameSpace>) => ({
  chats: {},
  notify: { type: "error", text: "" },
  setNotify: (notify?: {
    type: "error" | "success" | "warning";
    text: string;
  }) => set({ notify }),
  setChats: (chats: ChatType) => set({ chats }),
});

// @ts-ignore
store = devtools(store);
// @ts-ignore
export const useStore = create(store);
// @ts-ignore
export const useStorePersist = create(persist(store, { name: "chat313" }));

// اگر آبجکت شما بیش از اندازه پیچیده باشد نیاز دارید برای اینکه بی مورد استیت کال نشود از این روش استفاده کنید
// برای اینکه مقایسه دو آبجکت به درستی انجام شود
export const useStorePersistShallow = <U,>(selector: (state: any) => U) =>
  useStorePersist(selector, (state: U, newState: U) => {
    const compare = JSON.stringify(state) === JSON.stringify(newState);
    if (!compare) console.log("state", selector.toString());
    return compare;
  });

// @ts-ignore
export const getState = (key: string) => useStore.getState()[key];
// @ts-ignore
export const getStatePersist = (key: string) => useStorePersist.getState()[key];
