import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { QueryClient, QueryClientProvider } from "react-query";
import dynamic from "next/dynamic";
// @ts-ignore
import SnackbarProvider from "react-simple-snackbar";

const Notification = dynamic(
  () => import("../components/common/notification/Notification"),
  { ssr: false }
);

export default function App({ Component, pageProps }: AppProps) {
  const queryClient = new QueryClient();
  return (
    <main>
      <QueryClientProvider client={queryClient}>
        <Component {...pageProps} />
        <SnackbarProvider>
          <Notification />
        </SnackbarProvider>
      </QueryClientProvider>
    </main>
  );
}
