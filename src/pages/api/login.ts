import { useMutation } from "react-query";
import axios from "axios";
import { baseURL } from "@/pages/api/chat";

export const useLogin = () => {
  return useMutation(
    async ({ email, pass }: { email?: string; pass?: string }) => {
      if (!email || !pass || email.length < 4 || pass.length < 6) return;
      const body = new URLSearchParams();
      body.append("username", email);
      body.append("password", pass);
      const res = await axios({
        baseURL,
        url: `token`,
        headers: {
          "content-type": "application/x-www-form-urlencoded",
        },
        method: "post",
        data: body,
      });
      // if ([200, 201].indexOf(res.status) >= 0) {
      //   const setNotify = getState("setNotify");
      //   setNotify({ text: res?.data?.actionMessage });
      // }
      return res.data;
    }
  );
};

export const useForgotPass = () => {
  return useMutation(async ({ email }: { email?: string }) => {
    if (!email || email.length < 4) return;
    const res = await axios({
      baseURL,
      url: `user/forgotpass`,
      method: "post",
      data: {
        email,
      },
    });
    // if ([200, 201].indexOf(res.status) >= 0) {
    //   const setNotify = getState("setNotify");
    //   setNotify({ text: res?.data?.actionMessage });
    // }
    return res.data;
  });
};

export const useSignUp = () => {
  return useMutation(
    async ({ email, pass }: { email?: string; pass?: string }) => {
      if (!email || !pass || email.length < 4 || pass.length < 6) return;
      const res = await axios({
        baseURL,
        url: `user/signup`,
        method: "post",
        data: {
          email,
          pass,
        },
      });
      // if ([200, 201].indexOf(res.status) >= 0) {
      //   const setNotify = getState("setNotify");
      //   setNotify({ text: res?.data?.actionMessage });
      // }
      return res.data;
    }
  );
};
