import axios from "axios";
import { useMutation } from "react-query";
import Cookies from "js-cookie";
import Router from "next/router";

export const baseURL = "http://138.201.177.116:10000/";
// export const baseUrl ="http://localhost:1413/"
export const useChat = () => {
  return useMutation(async ({ text }: { text: string }) => {
    const auth = Cookies.get("auth");
    const res = await axios({
      baseURL,
      headers: {
        Authorization: `Bearer ${auth}`,
      },
      url: `send_prompt`,
      method: "post",
      params: {
        api_key: "chatgptIsTheBestForCallCenterAutomation",
        prompt: text,
      },
    }).catch((e) => {
      if (e.status === 403) {
        Router.replace("/login");
      }
    });
    return res?.data;
  });
};
